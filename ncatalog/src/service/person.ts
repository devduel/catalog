export class Person {

    name: string;
    surname: string;
    pesel: string;

    constructor(name: string, surname: string, pesel: string) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
    }

}