import { Module, Logger } from '@nestjs/common';
import { AppController } from './app.controller';
import { MockRepository, MockPESELService } from './service/mock-catalog';
import { Catalog } from './service/catalog';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [
    Logger,
    {
      provide: 'Repository',
      useClass: MockRepository
    },
    {
      provide: 'PESELService',
      useClass: MockPESELService
    },
    Catalog
  ],
})
export class AppModule { }
