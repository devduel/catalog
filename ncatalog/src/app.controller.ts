import { Controller, Get, Param, Logger } from '@nestjs/common';
import { AppService } from './app.service';
import { Catalog } from './service/catalog';

@Controller()
export class AppController {
    constructor(private readonly catalog: Catalog,
        private readonly log: Logger
    ) { }


    @Get("/pesel/:name,:surname")
    getPesel(@Param('name') name: string, @Param('surname') surname: string): String {
        this.log.debug("getPesel " + name + " : " + surname);
        return this.catalog.getPESEL(name, surname);
    }

    @Get("/person/:name,:surname,:pesel")
    addPerson(@Param('name') name: string,
        @Param('surname') surname: string,
        @Param('pesel') pesel: string): void {
        this.log.debug("addPerson " + name + " : " + surname + " : " + pesel);
        this.catalog.addPerson(name, surname, pesel);
    }
}
