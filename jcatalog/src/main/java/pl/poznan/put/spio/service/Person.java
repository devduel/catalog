package pl.poznan.put.spio.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Person {
    private final String name;
    private final String surname;
    private final String pesel;
}
