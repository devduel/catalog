package pl.poznan.put.spio;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.poznan.put.spio.db.DB;
import pl.poznan.put.spio.service.Catalog;
import pl.poznan.put.spio.service.PESELService;
import pl.poznan.put.spio.service.Person;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CatalogTest {

    @Mock
    private DB dbMock;

    @Mock
    private PESELService peselServiceMock;

    // Pokrywa się z zadaniem z punktu 3
    @Test
    public void shouldGetPersonByPesel() {
        // given
        Person person = new Person("imie", "nazwisko", "pesel");
        when(dbMock.getPerson("pesel")).thenReturn(person);// instruujemy zachowanie,
        // dla takiego wywołania, zwróć takie wartości

        Catalog c = new Catalog(dbMock, null);// tworzymy obiekt testowany,
        // spełniając zależność mockiem

        // when
        Person result = c.getPerson("pesel");// właściwy test

        // then
        assertEquals(person, result);// weryfikacja testu
        verify(dbMock, atLeastOnce()).getPerson("pesel");// dodatkowa
        // weryfikacja mocka
    }

    @Test
    public void shouldGetPeselForGivenName() {
        String name = "name";
        String surname = "surname";
        String pesel = "pesel";
        when(dbMock.getPESELList(name, surname)).thenReturn(Collections.singletonList(pesel));

        Catalog catalog = new Catalog(dbMock, null);

        String result = catalog.getPESEL(name, surname);

        assertEquals(pesel, result);
        verify(dbMock, atLeastOnce()).getPESELList(name, surname);
    }

    @Test
    public void shouldGetPeselListForGivenName() {
        String name = "name";
        String surname = "surname";
        List<String> pesels = Arrays.asList("pesel1", "pesel2", "pesel3");
        when(dbMock.getPESELList(name, surname)).thenReturn(pesels);

        Catalog catalog = new Catalog(dbMock, null);

        List<String> result = catalog.getPESELList(name, surname);

        assertAll("Check Pesel list",
                () -> assertEquals(pesels.size(), result.size()),
                () -> assertArrayEquals(pesels.toArray(), result.toArray()));
        verify(dbMock, atLeastOnce()).getPESELList(name, surname);
    }

    @Test
    public void shouldAddPerson() {
        Person person = new Person("name", "surname", "pesel");
        when(peselServiceMock.verify(person.getPesel())).thenReturn(true);

        Catalog catalog = new Catalog(dbMock, peselServiceMock);
        catalog.addPerson(person.getName(), person.getSurname(), person.getPesel());

        verify(peselServiceMock, atLeastOnce()).verify(person.getPesel());
        verify(dbMock, atLeastOnce()).insertPerson(person);
    }

    @Test
    public void shouldThrowExceptionWhenImproperPeselGiven() {
        Person person = new Person("name", "surname", "pesel");
        when(peselServiceMock.verify(person.getPesel())).thenReturn(false);

        Catalog catalog = new Catalog(null, peselServiceMock);

        assertThrows(RuntimeException.class, () -> catalog.addPerson(person.getName(), person.getSurname(), person.getPesel()));
    }
}